# Expectations for **Coding for Humanities**

This is an expectation list for the class „Coding for Humanities“ If you expected something else please click [here](http://www.emergencyyodel.com)

## Expectations /To Do:

* Learn to Code in R
* Learn to analyze various input data
* Learn to analyze datastructures and transfer it to human knowledge
* Download the internet
* Analyze the internet
* Summarize the whole internet and all public and secret databases
* Convert all the stuff to „*My_New_Wisdom.hkdb*“ - human knowledge database format -
* Transfer „*My_New_Wisdom.hkdb*“ to my Brain - or my Brain into a machine, whichever works faster 
* Become ruler of the known world
* Begin ruling of the world
* Make „Nazis Boxen“ compulsory sport and „All heil Koentges“ world religion
* Enter loop of:
* Do fun stuff
* Become bored
* Destroy stuff
* Repeat loop

* While you’re at it: get superpowers somehow